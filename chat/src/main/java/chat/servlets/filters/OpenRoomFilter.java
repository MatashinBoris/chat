package chat.servlets.filters;

import chat.utils.jwt.JWT;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/openRoom")
public class OpenRoomFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);
        if (session == null) {
            response.sendRedirect("/singIn");
            System.out.println("-------------------------------------------------");
            System.out.println("SESSION IS NULL");
            System.out.println("-------------------------------------------------");
            return;
        }

        if (session.getAttribute("nameOfRoom") == null && request.getParameter("openRoom") == null) {
            session.invalidate();
            System.out.println("-------------------------------------------------");
            System.out.println("session.getAttribute(\"nameOfRoom\") == " + session.getAttribute("nameOfRoom") );

            System.out.println("request.getParameter(\"openRoom\") == " + request.getParameter("openRoom"));
            System.out.println("-------------------------------------------------");
            response.sendRedirect("/singIn");
            return;
        }

        if (session.getAttribute("login") == null || !JWT.myJwtDecoder((String) session.getAttribute("token"))) {
            session.invalidate();

            System.out.println("session.getAttribute(\"login\") == " + session.getAttribute("login"));
            System.out.println("JWT.myJwtDecoder((String) session.getAttribute(\"token\")) ==="  + JWT.myJwtDecoder((String) session.getAttribute("token")));
            response.sendRedirect("/singIn");
            return;
        } else {

            session.setAttribute("token", JWT.updateJwt((String) session.getAttribute("token")));
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
