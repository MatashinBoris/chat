package chat.servlets.filters;

import chat.utils.jwt.JWT;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/createRoom")
public class CreateRoomFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        if (session == null || session.getAttribute("login") == null || !JWT.myJwtDecoder((String) session.getAttribute("token")) ||
                request.getParameter("secondUser") == null) {

            if (session != null) {
                session.invalidate();
            }
            servletRequest.getServletContext().getRequestDispatcher("/singIn").forward(request, response);
        } else {

            session.setAttribute("token", JWT.updateJwt((String) session.getAttribute("token")));
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
