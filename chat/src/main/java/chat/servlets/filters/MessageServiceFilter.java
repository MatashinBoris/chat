package chat.servlets.filters;

import chat.utils.jwt.JWT;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/messageService")
public class MessageServiceFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

//        HttpSession session = request.getSession(false);

//        if (session == null) {
//            response.sendRedirect("/singIn");
//            return;
//        }
//
//        if (session.getAttribute("login") == null || !JWT.myJwtDecoder((String) session.getAttribute("token"))
//                || request.getSession().getAttribute("nameOfRoom") == null || request.getParameter("smsLastTime") == null) {
//            session.invalidate();
//            response.sendRedirect("/singIn");
//            return;
//        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
