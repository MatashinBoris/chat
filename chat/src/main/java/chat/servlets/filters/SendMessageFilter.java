package chat.servlets.filters;

import chat.utils.jwt.JWT;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/sendMessage")
public class SendMessageFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {


        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        if (session == null) {
            System.out.println("-------------------------------------------------");
            System.out.println("SESSION IS NULL");
            System.out.println("-------------------------------------------------");
            response.sendRedirect("/singIn");
            return;
        }

        if (session.getAttribute("login") == null || !JWT.myJwtDecoder((String) session.getAttribute("token"))) {
            session.invalidate();
            System.out.println("session.getAttribute(\"login\") == " + session.getAttribute("login"));
            System.out.println("JWT.myJwtDecoder((String) session.getAttribute(\"token\")) ==="  + JWT.myJwtDecoder((String) session.getAttribute("token")));
            response.sendRedirect("/singIn");
            return;
        }

        String message = request.getParameter("message");
        String name = (String) session.getAttribute("nameOfRoom");
        String admin = (String) session.getAttribute("login");

        if (message != null || name != null || admin != null) {
            request.setAttribute("openRoom", name);
            session.setAttribute("token", JWT.updateJwt((String) session.getAttribute("token")));
        }


        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
