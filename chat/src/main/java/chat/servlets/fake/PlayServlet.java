package chat.servlets.fake;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

@WebServlet(name = "music", urlPatterns = {"/play"})
public class PlayServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*   resp.sendRedirect("music.jsp");*/


        getServletContext().getRequestDispatcher("/music.jsp").forward(req, resp);
        /*resp.sendRedirect("music.jsp");*/

        PrintWriter out = resp.getWriter();


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.print((String) req.getParameter("secondUser"));
        out.print((String)req.getSession().getAttribute("login"));
    }
}
