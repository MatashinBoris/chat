package chat.servlets.userSteps;

import chat.database.workDatabase.RoomService;
import chat.dto.MessageCheck;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "MessageServiceServlet", urlPatterns = {"/messageService"})
public class MessageServiceServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Gson gson = new Gson();

        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Credentials", "true");
        resp.setHeader("Access-Control-Max-Age", "1800");
        resp.setHeader("Access-Control-Allow-Headers", "content-type");
        resp.setHeader("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, PATCH, OPTIONS");



        MessageCheck check = gson.fromJson(httpBodyToString(req), MessageCheck.class);
        check.setNameOfRoom(check.getNameOfRoom().trim());

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        PrintWriter out = resp.getWriter();
        Date date;
        try {

             date = sdf.parse(check.getSmsLastTime());

        } catch (ParseException e) {
            e.printStackTrace();
            out.write("false");
            req.getSession().invalidate();
            return;
        }

        RoomService service = new RoomService();

        if (service.getLastMessages(date, check.getNameOfRoom())) {
            System.out.println("------------------------------------");
            System.out.println(date.toString());
            System.out.println("true");
            System.out.println("------------------------------------");
            out.write("true");
            resp.setStatus(HttpServletResponse.SC_OK);
        } else {
            System.out.println("------------------------------------");
            System.out.println("false");
            System.out.println("------------------------------------");
            out.write("false");
            resp.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }

    }

    public String httpBodyToString(HttpServletRequest request) throws IOException {

        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
        return stringBuilder.toString();
    }
}
