package chat.servlets.userSteps;

import chat.database.actions.UserSteps;
import chat.database.workDatabase.RoomService;
import chat.database.workDatabase.UserService;
import chat.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "CreateRoomServlet", urlPatterns = {"/createRoom"})
public class CreateRoomServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String secondUser = req.getParameter("secondUser");
        String admin = (String) req.getSession().getAttribute("login");

        UserService userService = new UserService();
        RoomService roomService = new RoomService();
        User currentUser = userService.getUserByName(admin);


        UserSteps steps = new UserSteps();

        if (roomService.getRoomByName(admin + "_" + secondUser) != null || roomService.getRoomByName(secondUser + "_" + admin) != null) {

            HttpSession session = req.getSession(false);
            session.setAttribute("error", "Name of room is busy");
        } else {
            steps.addNewRoom(currentUser, admin + "_" + secondUser, secondUser);
        }

        resp.sendRedirect("/chat");

    }
}
