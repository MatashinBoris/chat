package chat.servlets.userSteps;

import chat.database.actions.UserSteps;
import chat.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SendMessageServlet", urlPatterns = {"/sendMessage"})
public class SendMessageServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String message = req.getParameter("message");
        String name = (String) req.getSession(false).getAttribute("nameOfRoom");
        String admin = (String) req.getSession(false).getAttribute("login");

        UserSteps steps = new UserSteps();
        steps.sendMessage(new User(admin), name, message);

        req.setAttribute("openRoom", name);

        resp.sendRedirect("/openRoom");

    }
}
