package chat.servlets.userSteps;

import chat.database.actions.UserSteps;
import chat.model.Room;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "OpenRoom", urlPatterns = {"/openRoom"})
public class OpenRoomServlet extends HttpServlet {

    private final UserSteps steps = new UserSteps();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);

        String nameOfRoom = (String) session.getAttribute("nameOfRoom");
        Room room = steps.openRoom(nameOfRoom);


        session.setAttribute("haveRoom", "yes");
        session.setAttribute("nameOfRoom", room.getName());
        session.setAttribute("RoomMessages", room.getMessages());


        resp.sendRedirect("/chat");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);

        String nameOfRoom = req.getParameter("openRoom");

        Room room = steps.openRoom(nameOfRoom);
        if (room == null && nameOfRoom != null) {
            resp.sendRedirect("/logOut");
            session.invalidate();
        } else {

            session.setAttribute("haveRoom", "yes");
            session.setAttribute("nameOfRoom", room.getName());
            session.setAttribute("RoomMessages", room.getMessages());
        }

        resp.sendRedirect("/chat");
    }
}
