package chat.servlets;

import chat.database.workDatabase.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

@WebServlet(name = "chatPage", urlPatterns = "/chat")
public class ChatPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserService userService = new UserService();

        req.setAttribute("users", userService.getAllUsers());
        AtomicReference<String> q = new AtomicReference<>();
        new Thread(() -> q.set(methodOLOLO())).start();

        System.out.println(q);
        req.setAttribute("rooms", userService.getAllRoomsByUser((String) req.getSession(false).getAttribute("login")));
        getServletContext().getRequestDispatcher("/mainPage.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/mainPage.jsp").forward(req, resp);
    }

    private String methodOLOLO ( ){

        return "NEW Q Q ";
    }
}
