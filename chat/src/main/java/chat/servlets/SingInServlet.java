package chat.servlets;

import chat.database.workDatabase.UserService;
import chat.model.User;
import chat.utils.jwt.JWT;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "SingIn", urlPatterns = "/singIn")
public class SingInServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getSession(false).invalidate();
        getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String login = req.getParameter("login");
        String password = req.getParameter("password");

        UserService service = new UserService();

        if (service.verifyUser(new User(login, password))) {

            HttpSession session = req.getSession();
            session.setAttribute("login", login);
            session.setAttribute("token", JWT.createJWT(session.getId(), "UserWriter", login, 600000));
            session.setMaxInactiveInterval(-1);
            resp.sendRedirect("/chat");
        } else {

            req.setAttribute("verify", "invalid data");
            getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);


        }
    }

}
