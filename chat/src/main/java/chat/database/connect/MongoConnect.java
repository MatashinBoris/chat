package chat.database.connect;

import chat.model.Message;
import chat.model.Room;
import chat.model.User;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.pojo.PojoCodecProvider;

public class MongoConnect {
    private static final String Mongo_uri = "mongodb+srv://chater:chater@secretchat.ru9eg.mongodb.net/test";

    public MongoCollection<User> getUsersCollection() {
        return getDBConnection().getCollection("users", User.class);
    }

    public MongoCollection<Room> getRoomsCollection() {
        return getDBConnection().getCollection("rooms", Room.class);
    }

    public MongoCollection<Message> getMessagesCollection() { return getDBConnection().getCollection("messages", Message.class); }

    private MongoDatabase getDBConnection() {

        return new MongoClient(new MongoClientURI(Mongo_uri))
                .getDatabase("Chat")
                .withCodecRegistry(CodecRegistries.fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(PojoCodecProvider.builder()
                                .automatic(true)
                                .build())
                ));
    }

}
