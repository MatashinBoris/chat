package chat.database.actions;

import chat.database.workDatabase.RoomService;
import chat.database.workDatabase.UserService;
import chat.model.Message;
import chat.model.Room;
import chat.model.User;
import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserSteps {
    private final RoomService roomService = new RoomService();
    private final UserService userService = new UserService();


    public Room openRoom(String name){
       return roomService.getRoomByName(name);
    }

    public Room createNewRoom(User admin, User secondUser, String nameOfRoom){

        if (roomService.getRoomByName(nameOfRoom)==null){
            return null;
        } else {
           return roomService.insertNewRoom(admin,nameOfRoom);
        }
    }

    public Boolean addNewRoom(User admin, String name, String LoginOfSecondUser){
        if (roomService.getRoomByName(name) == null){

            Room newRoom = roomService.insertNewRoom(admin, name);
            User secondUser = userService.getUserByName(LoginOfSecondUser);


            if(secondUser != null){

                if (secondUser.getRooms() == null){
                    List<ObjectId> rooms = new ArrayList<>();
                    rooms.add(newRoom.getId());
                    secondUser.setRooms(rooms);
                    userService.updateUser(secondUser);
                }
                else {
                    List<ObjectId> rooms =  secondUser.getRooms();
                    rooms.add(newRoom.getId());
                    secondUser.setRooms(rooms);
                    userService.updateUser(secondUser);
                }

                if (admin.getRooms() == null){
                    List<ObjectId> rooms = new ArrayList<>();
                    rooms.add(newRoom.getId());
                    admin.setRooms(rooms);
                    userService.updateUser(admin);
                }
                else {
                    List<ObjectId> rooms = admin.getRooms();
                    rooms.add(newRoom.getId());
                    admin.setRooms(rooms);
                    userService.updateUser(admin);
                }
                return true;
            }
        }
        return false;
    }

    public Room addUserToRoom(Room room, User newUser){
        roomService.getRoomByName(room.getName());
        newUser.getRooms().add(room.getId());
        userService.updateUser(newUser);

        return room;
    }

    public boolean sendMessage(User user, String roomName, String text){

        try {
            Room room = roomService.getRoomByName(roomName);
            room.getMessages().add(new Message(user.getLogin(), text, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()),new Date().getTime(), room.getId()));
            roomService.updateRoom(room);
            return true;
        }
        catch (Exception e){
            return false;
        }
    }
}
