package chat.database.workDatabase;

import chat.database.connect.MongoConnect;
import chat.model.Room;
import chat.model.User;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.and;

public class UserService {

    private final MongoConnect connect = new MongoConnect();

    public User updateUser(User user) {
        connect.getUsersCollection().replaceOne(Filters.eq("_id", user.getId()), user);

        return connect.getUsersCollection().
                find(and(Filters.eq("login", user.getLogin()), Filters.eq("password", user.getPassword())))
                .first();
    }

    public boolean insertUser(User user) {
        try {
            connect.getUsersCollection().insertOne(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Room> getAllRoomsByUser(String name){
        return new RoomService().getRoomsById(getUserByName(name).getRooms());
    }

    public User getUserByName(String name){
        return connect.getUsersCollection().
                find(and(Filters.eq("login", name)))
                .first();
    }

    public boolean verifyUser(User user) {

        User verifiedUser = connect.getUsersCollection().
                find(and(Filters.eq("login", user.getLogin()), Filters.eq("password", user.getPassword())))
                .first();
        return verifiedUser != null;
    }

    public User signIn(User user) {

        if (verifyUser(user)) {
            return connect.getUsersCollection().
                    find(and(Filters.eq("login", user.getLogin()), Filters.eq("password", user.getPassword())))
                    .first();
        } else return new User("not registered", "not registered");

    }

    public List<User> getAllUsers() {

        List<User> list = new ArrayList<>();
        MongoCollection<User> collection = connect.getUsersCollection();

        try (MongoCursor<User> cursor = collection.find().iterator()) {
            while (cursor.hasNext()) {
                list.add(cursor.next());
            }
        }
        return list;

    }

}
