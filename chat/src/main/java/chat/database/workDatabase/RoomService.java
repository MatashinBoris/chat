package chat.database.workDatabase;

import chat.database.connect.MongoConnect;
import chat.model.Message;
import chat.model.Room;
import chat.model.User;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Filters.and;

public class RoomService {
    private final MongoConnect connect = new MongoConnect();

    public boolean getLastMessages(Date date, String nameOfRoom)  {
        List<Message> messages;
        try {
            messages = connect.getRoomsCollection().find(Filters.eq("name", nameOfRoom)).first().getMessages();
        } catch (NullPointerException e) {
            return false;
        }

        for (Message message : messages) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                    Date messagesDate = null;
                  try {
                        messagesDate = sdf.parse(message.getTime());
                  } catch (ParseException e) {
                        e.printStackTrace();
                  }

            if (date.before(messagesDate)) {
                return true;
            }
        }
        return false;
    }

    public void updateRoom(Room room) {
        connect.getRoomsCollection().updateOne(Filters.eq("name", room.getName()), new Document("$set", room));
    }

    public Room insertNewRoom(User admin, String name) {

        List<Message> startMessages = new ArrayList<>();
        startMessages.add(
                new Message(admin.getLogin(), admin.getLogin() + " created this conversation",
                        new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()), new Date().getTime()));


        connect.getRoomsCollection().insertOne(new Room(name, startMessages));

        return connect.getRoomsCollection().
                find(and(Filters.eq("name", name)))
                .first();
    }

    public Room getRoomByName(String name) {

        return connect.getRoomsCollection().
                find(and(Filters.eq("name", name)))
                .first();
    }

    public List<Room> getAllRooms() {

        List<Room> list = new ArrayList<>();
        MongoCollection<Room> collection = connect.getRoomsCollection();

        try (MongoCursor<Room> cursor = collection.find().iterator()) {
            while (cursor.hasNext()) {
                list.add(cursor.next());
            }
        }
        return list;
    }


    public List<Room> getRoomsById(List<ObjectId> listId) {


        List<Room> allRooms = getAllRooms();
        List<Room> userRooms = new ArrayList<>();

        for (ObjectId objectId : listId) {

            for (Room allRoom : allRooms) {
                if (objectId.equals(allRoom.getId()))
                    userRooms.add(allRoom);
            }
        }

        return userRooms;
    }
}
