package chat.dto;

public class MessageCheck {

    private String smsLastTime;
    private String nameOfRoom;

    public String getSmsLastTime() {
        return smsLastTime;
    }

    public void setSmsLastTime(String smsLastTime) {
        this.smsLastTime = smsLastTime;
    }

    public String getNameOfRoom() {
        return nameOfRoom;
    }

    public void setNameOfRoom(String nameOfRoom) {
        this.nameOfRoom = nameOfRoom;
    }
}
