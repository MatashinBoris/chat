package chat.utils.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

public class JWT {

    private static String SECRET_KEY = "oeRaYY7Wo24sDqKSX3IM9ASGmd213123qwddwqrWEQWEQSdaxc1GPmkT12$@#@!%SQQxcqwsdfqd9jo1QTy4b7481qwWQXZCVETQW*($!EZBEWRqP9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOy21321qEGhXEbdJI-ZQ19k_o9MI0y3eZN2lp9jqdwq213ow55FfXMi31231221INEdt1XR85VipRLSOkT6kSpzs2x-jbLDiz9iFVzkd81YKxMgPA7VfZeQUm4n-mOmnWMaVX30zGFU4L3oPBctYKkl4dYfqYWqRNfrgPJVi5DGFjywgxx0@!#qdw123ASEiJHtV72paI3fDR2XwWDQ@#!#$CFAC!@#!@lSkyhhmY-ICjCRmsJN4fX1qwdqw@E@! Qwd212pdoL8a18-aQrvyu4j0Os6dVPYIoPvvY0Sewqeq wAZtWYKHfM1WQEE211sz5g7A3HD4cVREf9cUsprCRK93w";


    public static String updateJwt(String oldJwt){

        Claims claims = JWT.decodeJWT(oldJwt);
        return JWT.createJWT(claims.getId(),claims.getIssuer(),claims.getSubject(),600000);
    }

    //Sample method to construct a JWT
    public static String createJWT(String id, String issuer, String subject, long ttlMillis) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public static Claims decodeJWT(String jwt) {

        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

    public static Boolean myJwtDecoder(String jwt){
        try{
            decodeJWT(jwt);
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
