package chat.model;

import org.bson.types.ObjectId;

import java.util.List;

public class Room {
    private ObjectId id;
    private String name;
    private List<Message> messages;


    public Room() {

    }

    public Room(String name) {
        this.name = name;
    }

    public Room(String name, List<Message> messages) {
        this.name = name;
        this.messages = messages;
    }

    public Room(ObjectId id, String name, List<Message> messages) {
        this.id = id;
        this.name = name;
        this.messages = messages;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
