package chat.model;

import org.bson.types.ObjectId;

import java.util.List;

public class User {
    private ObjectId id;
    private String login;
    private String password;
    private List<ObjectId> rooms;

    public User() {

    }

    public User(String login) {
        this.login = login;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User(ObjectId id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    public User(ObjectId id, String login, String password, List<ObjectId> rooms) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.rooms = rooms;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<ObjectId> getRooms() {
        return rooms;
    }

    public void setRooms(List<ObjectId> rooms) {
        this.rooms = rooms;
    }
}
