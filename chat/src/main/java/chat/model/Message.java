package chat.model;

import org.bson.types.ObjectId;

public class Message {
    private ObjectId id;
    private String owner;
    private String text;
    private String time;
    private long milliseconds;
    private ObjectId roomId;

    public Message() {

    }

    public Message(String owner, String text, String time , long milliseconds) {
        this.owner = owner;
        this.text = text;
        this.time = time;
        this.milliseconds = milliseconds;
    }

    public Message(String owner, String text, String time, long milliseconds , ObjectId roomId) {
        this.owner = owner;
        this.text = text;
        this.time = time;
        this.roomId = roomId;
        this.milliseconds = milliseconds;
    }

    public long getMilliseconds() {
        return milliseconds;
    }

    public void setMilliseconds(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ObjectId getRoomId() {
        return roomId;
    }

    public void setRoomId(ObjectId roomId) {
        this.roomId = roomId;
    }
}
