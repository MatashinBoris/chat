package test;

import chat.database.workDatabase.UserService;
import chat.dto.MessageCheck;
import chat.model.Message;
import chat.model.User;
import chat.utils.crypt.PBKDF2Hasher;
import chat.utils.jwt.JWT;
import com.google.gson.Gson;

import io.jsonwebtoken.Claims;

import java.util.Date;

import static com.mongodb.internal.connection.tlschannel.util.Util.assertTrue;

public class Test {
    private static final PBKDF2Hasher mPBKDF2Hasher = new PBKDF2Hasher();;
    public static void main(String[] args) {
/*        UserService service = new UserService();

       // service.insertUser(new User("Leha", "Sim"));

        User use = service.signIn(new User("Leha", "Sim"));
        System.out.println(use.getId() + "  " + use.getLogin() + "  " + use.getPassword());

        JWT jwt = new JWT();
        Gson gson = new Gson();
        System.out.println(JWT.createJWT("her12","issuer",gson.toJson(use),500000));
        */

        //System.out.println(check("passwordId"));

       /* UserService service = new UserService();
        service.insertUser(new User("Boris","boris"));
        service.insertUser(new User("Dmitriy","dmitriy"));
        service.insertUser(new User("lex","lex"));*/

       /* String password = "passwordId";

        String hash1 = "$31$16$ODkB_l2B0EyOLGCWlbfKk9FSwIo2_rfRAay9C_jf5ww";

        String has1 = "$31$16$d93ImQfhdZ8-qWJOTSaFPN-IoPPTJHFoOXcXKYyRsds";

        System.out.println(mPBKDF2Hasher.checkPassword(password.toCharArray(),hash1)? "ok" : "bad");

        System.out.println(mPBKDF2Hasher.checkPassword(password.toCharArray(),has1)? "ok" : "bad");*/

      //  String jwt = JWT.createJWT("id12","ChaterGo","Login",800000);
        // System.out.println(JWT.createJWT("id12","ChaterGo","Login",800000));

      //  System.out.println("-------------------");
       // System.out.println(JWT.myJwtDecoder("eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJpZDEyIiwiaWF0IjoxNjA1OTg0MDAxLCJzdWIiOiJMb2dpbiIsImlzcyI6IkNoYXRlckdvIiwiZXhwIjoxNjA1OTg0ODAxfQ.xsK-164OZ6xtlV2pM4n7g1lCgiHXDbnfOLjQmJRCZrU"));

         //   UserService service = new UserService();
     //   System.out.println(service.getUserByName("Boris").getLogin() + "  " + service.getUserByName("Boris").getPassword());
       // System.out.println( service.getUserByName("Leha").getLogin() + service.getUserByName("Leha").getPassword());

    }




    public static String check(String hash){

       return mPBKDF2Hasher.hash(hash.toCharArray());

    }


    public void givenCorrectMessageAndHash_whenAuthenticated_checkAuthenticationSucceeds()  {
        String message1 = "password123";

        String hash1 = mPBKDF2Hasher.hash(message1.toCharArray());

        assertTrue(mPBKDF2Hasher.checkPassword(message1.toCharArray(), hash1));

    }


    public void givenWrongMessage_whenAuthenticated_checkAuthenticationFails(){
        String message1 = "password123";

        String hash1 = mPBKDF2Hasher.hash(message1.toCharArray());

        String wrongPasswordAttempt = "IamWrong";

       // assertFalse(mPBKDF2Hasher.checkPassword(wrongPasswordAttempt.toCharArray(), hash1));

    }
}
