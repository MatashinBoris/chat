<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Musical Service</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Literata:wght@200&display=swap" rel="stylesheet">
</head>
<body>


<div class="wrapper">


    <div class="main-reg">

        <div class="down">
            <form class="enter" action="/singIn" method="post">
                <input class="inputTag" type="text" name="login" placeholder="Login">
                <input class="inputTag" type="text" name="password" placeholder="Password"/>
                <input class="inputTagOK" type="submit" value="Sign In"/>
                <div class="answer-div">
                <p> <% String errorResponse = (String) request.getAttribute("verify");
                        if (errorResponse != null) {
                        out.print("<p class=\"invalidResponse>" + errorResponse +"</p>");
                      }%></p>
                </div>
            </form>
        </div>

    </div>

</div>

</body>
</html>
