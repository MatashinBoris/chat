<%@ page import="chat.model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="chat.model.Room" %>
<%@ page import="chat.model.Message" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>--%>
<html>
<head>
    <title>Musical Service</title>

    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/media.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Literata:wght@200&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="assets/main.js"></script>
</head>
<body>
<div class="wrapper-chat">
    <header>
        <header>

            <% List<Room> rooms = (List<Room>) request.getAttribute("rooms"); %>

            <div class="dropdown responsiveRooms" style="float:left;">
                <button class="dropbtn buttonResponse">Rooms</button>



                <div class="dropdown-content" style="left:0;">

                    <% if (rooms != null) {
                        for (Room room : rooms) { %>
                    <div class="user_item">

                        <form class="form_users_dropdown" action="${pageContext.request.contextPath}/openRoom"
                              method="post">
                            <input class="users_add_chat" type="submit" name="openRoom"
                                   value="<%out.print(room.getName());%>">
                        </form>

                    </div>

                    <%
                        }
                    } else {
                    %>
                    <div class="room">
                        <p><% out.print("click \"add new room\"");%></p>
                    </div>

                    <%}%>



                </div> <!-- dropdown-content -->
            </div>
            <!-- dropdown-users -->
            <% List<User> users = (List<User>) request.getAttribute("users"); %>

            <div class="dropdown" style="float:left;">
                <button class="dropbtn buttonResponse addRoomResp">new room</button>
                <div class="dropdown-content" style="left:0;">

                    <%for (int i = 0; i < users.size(); i++) {
                            if (!users.get(i).getLogin().equals(session.getAttribute("login"))) {%>
                    <div class="user_item">
                        <form class="form_users_dropdown" action="${pageContext.request.contextPath}/createRoom"
                              method="post">
                            <input class="users_add_chat" type="submit" name="secondUser"
                                   value="<%out.print(users.get(i).getLogin());%>">
                        </form>
                    </div>
                    <%} }%>
                </div> <!-- dropdown-content -->
            </div><!-- dropdown users-->

            <div>
                <p id="error" class="errorPlace"><%

                    if (session.getAttribute("error") != null && session.getAttribute("error").equals("Name of room is busy")) {
                        out.print("Name of room is busy");
                        session.removeAttribute("error");
                    }%></p>
            </div>

            <div class="log_out">
                <form class="log_out_form" action="${pageContext.request.contextPath}/logOut">
                    <input type="submit" value="Log Out">
                </form>
            </div>

        </header>
    </header>

    <div class="main-place">
        <div class="convers">


            <div class="rooms">

                <% if (rooms != null) {
                    for (Room room : rooms) { %>
                <div class="room">
                    <form class="form_users_dropdown " action="${pageContext.request.contextPath}/openRoom"
                          method="post">
                        <input class="users_add_chat rooms_list" type="submit" name="openRoom"
                               value="<%out.print(room.getName());%>">
                    </form>
                </div>

                <%
                    }
                } else {
                %>
                <div class="room">
                    <p><% out.print("click \"add new room\"");%></p>
                </div>

                <%}%>
            </div>
        </div>


        <div class="mess">
            <% if (session.getAttribute("haveRoom") == null) {%>
            <div class="prepareToConversation">
                <p class="chooseRoomText">Выберете, кому хотели бы написать</p>
            </div>

            <% } else {%>

            <div  id="scroll" class="main-chat-data">
                <div class="info">
                    <h3 id="RoomNameId">
                        <% String nameOfRoom = (String) session.getAttribute("nameOfRoom");
                            if (nameOfRoom != null)
                                out.print(nameOfRoom);
                        %><%--Raznaya info about room of objects--%>
                    </h3>
                    <hr>
                </div>
                <div class="messages-body">
                    <% List<Message> messages = (List<Message>) session.getAttribute("RoomMessages"); %>


                    <%  if (messages!= null){
                        for (int i = 0; i < messages.size() ; i++) { %>
                    <div class="message<% if (messages.get(i).getOwner().equals(session.getAttribute("login"))){
                        out.print(" ownerMessage");
                    }%>">

                        <div class="info-sms">
                            <b class="nameOfUserSms"><% out.print(messages.get(i).getOwner());%></b>
                            <p class="timeSms"><% out.print(messages.get(i).getTime());%></p>

                        </div>
                        <p class="textSms"><% out.print(messages.get(i).getText());%></p>
                    </div>

                    <%  }}%>
                </div>
            </div>

            <div class="sender">

                <div class="container">
                    <form action="${pageContext.request.contextPath}/sendMessage" class="form-send" method="post">
                        <div class="row row1">
                            <div class="col-75">
                                <textarea id="subject" name="message" placeholder="send me something.."
                                          style="height:95px"></textarea>
                            </div>
                        </div>
                        <div class="row row2">
                            <input class="sendMessageInput" type="submit" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
            <%}%>
        </div>

    </div>
</div>

</body>

</html>
